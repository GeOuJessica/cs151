//import java.util.ArrayList;
//import java.util.Scanner;

public class User {
    public enum Type {
        INSTRUCTOR, STUDENT
    };
	private String username;
	private String password;
	private boolean instructor;
	
	
	public User(String username, String password, Type type) {
		this.username = username;
		this.password = password;
		instructor = type == Type.INSTRUCTOR;
	}
	
/*	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setUsername() {
		this.username = username;
	}
	
	public void setPassword() {
		this.password = password;
	}
*/
// Whether or not input equal to person who has already in
	public boolean authenticate(String username2, String password2) {
		return this.username.equals(username2) && this.password.equals(password2);
		
	}
	
	
	public boolean isInstructor() {
		return instructor;
	}
	public String toString() {
		return username;
	}

}
