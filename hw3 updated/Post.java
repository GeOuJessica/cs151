import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Post extends RatedSubmission{
   private Response studentResponse = new Response();
   private Response instructorResponse = new Response(); 
   private Set<User> viewers = new HashSet<>(); 
   private List<FollowupDiscussion> followups = new ArrayList<>(); 



	public Response getInstructorResponse() {
		return instructorResponse;
	}

	public Response getStudentResponse() {
		return studentResponse;
	}

	public int view(User currentUser) {
		viewers.add(currentUser);
		return viewers.size();
	}

	public int add(FollowupDiscussion followup) {
		followups.add(followup);
		return followups.size() - 1;
	}
	
	public FollowupDiscussion getFollowup(int i) {
		return followups.get(i);
	}

	public int getFollowupCount() {
		return followups.size();
	}

}
	