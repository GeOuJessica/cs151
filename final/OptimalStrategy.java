class OptimalStrategy
{
    public static void play(Game<Integer> game, Strategy<Integer> strategy)
    {
        Scanner in = new Scanner(System.in);
        Random gen = new Random();
        boolean humanTurn = gen.nextBoolean();
        Integer p = game.initialState(gen);
        System.out.println("Initial position " + p);
        while (!game.win(p))
        {
            if (humanTurn)
            {
                List<Integer> nextMoves = game.move(p);
                System.out.print("Next move? " + nextMoves + " ");
                p = in.nextInt();
                if (!nextMoves.contains(p))
                {
                    System.out.println("Not a legal move--you lose");
                    return;
                }
            }
            else
            {
                p = strategy.nextMove(game, p);
                if (p == null)
                {
                    System.out.println("I lose");
                    return;
                }
                else System.out.println("I move to " + p);
            }
            humanTurn = !humanTurn;
        }
        System.out.println("You " + (humanTurn ? "lose" : "win"));
    }
}
