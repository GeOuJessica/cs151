class OptimalStrategyTest implements Game<Integer>
{
    public List<Integer> move(Integer p)
    {
        if (p > 2) return List.of(p - 2, p - 1);
        else if (p == 2) return List.of(1);
        else return List.of();
    }
    public boolean win(Integer p)
    {
        return p == 1;
    }
    public Integer initialState(Random gen) { return 20 + gen.nextInt(80); }
}
