'use strict'

function drawGrabber(x, y) {
  const size = 5;
  const canvas = document.getElementById('graphpanel')
  const ctx = canvas.getContext('2d') // No need for "if (canvas.getContext)"
  ctx.fillStyle = 'black'
  ctx.fillRect(x - size / 2, y - size / 2, size, size)
}

function createCircleNode (x, y, size, color) {
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: size,
        height: size
      }
    },
    contains: p => {
      return (x + size / 2 - p.x) ** 2 + (y + size / 2 - p.y) ** 2 <= size ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    draw: () => {
      const container = document.getElementById('nodeContainer')
      const table = document.createElement('table')
      const tr = document.createElement('tr')
      const td = document.createElement('td')
      table.appendChild(tr)
      tr.appendChild(td)
      table.style.position = 'absolute'
      table.style.left = x + 'px'
      table.style.top = y + 'px'
      table.style.width = size + 'px'
      table.style.height = size + 'px'
      table.style.background = color
      container.appendChild(table)
    }
  }
}

function center(rect) {
  return { x: rect.x + rect.width / 2, y: rect.y + rect.height / 2 }
}

function createLineEdge() {
  let start = undefined
  let end = undefined
  return {
    connect: (s, e) => {
      start = s
      end = e
    },
    draw: () => {
      const canvas = document.getElementById('graphpanel')
      const ctx = canvas.getContext('2d')
      ctx.beginPath()
      const p = center(start.getBounds()) // Just pick the center of the bounds for now
      const q = center(end.getBounds()) // Not the "connection points" that graphed2 uses
      ctx.moveTo(p.x, p.y)
      ctx.lineTo(q.x, q.y)
      ctx.stroke()
    }
  }
}

class Graph {
  constructor() {
    this.nodes = []
    this.edges = []
  }
  add(n) {
    this.nodes.push(n)
  }
  findNode(p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  connect(e, p1, p2) {
    const n1 = this.findNode(p1)
    const n2 = this.findNode(p2)
    if (n1 !== undefined && n2 !== undefined) {
      e.connect(n1, n2)
      this.edges.push(e)
      return true
    }
    return false
  }
  draw() {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const e of this.edges) {
      e.draw()
    }
  }
}

document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  const n1 = createCircleNode(10, 10, 20, 'goldenrod')
  const n2 = createCircleNode(30, 30, 20, 'blue')
  const n3 = createCircleNode(100, 100, 20, 'pink')
  const n4 = createCircleNode(80, 70, 20, 'black')
  graph.add(n1)
  graph.add(n2)
  graph.add(n3)
  graph.add(n4)
  const e1 = createLineEdge()
  const e2 = createLineEdge()
  graph.connect(e1, { x: 20, y: 20 }, { x: 40, y: 40 })
  graph.connect(e2, { x: 110, y: 110 }, { x: 90, y: 80 })

  graph.draw()
  
  const panel = document.getElementById('graphpanel')
  let selected = undefined
  let dragStartPoint = undefined
  let dragStartBounds = undefined
  

  function repaint() {
    
    const nodeContainer = document.getElementById('nodeContainer')
    nodeContainer.innerHTML = ''
    
    const canvas = document.getElementById('graphpanel')
    const ctx = canvas.getContext('2d') // No need for "if (canvas.getContext)"

    ctx.clearRect(0, 0, canvas.width, canvas.height)

    graph.draw()
    if (selected !== undefined) {
            const bounds = selected.getBounds()
            drawGrabber(bounds.x, bounds.y)
            drawGrabber(bounds.x+2, bounds.y+2)
            drawGrabber(bounds.x + bounds.width, bounds.y)
            drawGrabber(bounds.x + bounds.width -2, bounds.y + 2)
            drawGrabber(bounds.x, bounds.y + bounds.height)
            drawGrabber(bounds.x +2, bounds.y + bounds.height-2)
            drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
            drawGrabber(bounds.x + bounds.width -2, bounds.y + bounds.height-2)
    }    
  }
  
  function mouseLocation(event) {
    var rect = panel.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top,
    }
  }
  
  panel.addEventListener('mousedown', event => {
    let mousePoint = mouseLocation(event)
    selected = graph.findNode(mousePoint)
    if (selected !== undefined) {
      dragStartPoint = mousePoint
      dragStartBounds = selected.getBounds()
    }
    repaint()
  })

  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === undefined) return
    let mousePoint = mouseLocation(event)
    if (selected !== undefined) {
      const bounds = selected.getBounds();
      
      selected.translate(
        dragStartBounds.x - bounds.x 
          + mousePoint.x - dragStartPoint.x,
        dragStartBounds.y - bounds.y 
          + mousePoint.y - dragStartPoint.y);
      repaint()
    }
  })
  
  panel.addEventListener('mouseup', event => {
    dragStartPoint = undefined
    dragStartBounds = undefined
  })
})
