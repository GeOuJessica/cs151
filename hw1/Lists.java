import java.util.ArrayList;

public class Lists
{
    public static void swapLargestAndSecondSmallest(ArrayList<Integer> a)
    {
        int largest = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > a.get(largest)) {
                largest = i;
            }
        }
        int first, second;
        first = second = Integer.MAX_VALUE;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) < first) {
                second = first;
                first = a.get(i);
            } else if (a.get(i) < second && a.get(i) != first)
                second = a.get(i);
        }
        int secondSmallest = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) == second) {
                secondSmallest = i;
                break;
            }
        }
        int temp = a.get(largest);
        a.set(largest, a.get(secondSmallest));
        a.set(secondSmallest, temp);
    }
}

