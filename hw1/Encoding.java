import java.util.*;

public class Encoding
{
   public static Set<String> morseCodes(int m, int n)
   {
      Set<String> result = new TreeSet<>();
      generate(m,n,"",result);
      return result;
   }
   public static void generate(int m,int n,String generate,Set<String> result)
   {
      if(m==0&&n==0)
      {
         result.add(generate);
         return;
      }
      if(m==0)
      {
         generate(m,n-1,generate+"-",result);
         return;
      }
      if(n==0)
      {
         generate(m-1,n,generate+".",result);
         return;
      }
      generate(m,n-1,generate+"-",result);
      generate(m-1,n,generate+".",result);
   }
}