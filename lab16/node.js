'use strict'

function createCircleNode (x, y, size, color) {
	return {
		getBounds: () => {
			return {
				x: x,
				y: y,
				width: size,
				height: size
				}
			},
	contain: p => {
		return (x+ size / 2 - p.x) **2 +(y + size /2 - p.y) ** 2 <= size ** 2/4
	},
	translate: (dx, dy) => {
		x += dx
		y += dy
		}
	}
}

module.exports = {createCircleNode}