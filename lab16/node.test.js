'use strict'

const { createCircleNode} = require ('./node')
const n = createCircleNode (10, 10, 20, 'blue')

test('getBounds', () => {
	expect(n.getBounds().width).toBe(20)
})

test('contains', () => {
	expect(n.contains({x: 12, y: 12})).toBe(false)
	expect(n.contains({x: 22, y: 22})).toBe(true)
})

test('translate', () => {
	n.translate(5, 20)
	expect(n.getBounds().x).toBe(15)
})