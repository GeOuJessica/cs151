import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square {
    constructor(color, text) {
        this.color = color
        this.text = text
    }
    getProperties() { return [
                              {name: 'color', value: this.color },
                              {name: 'text', value: this.text }] }
}

class Sheet extends React.Component {
    render() {
        return (<table>{this.props.dataProps.map(p => <Row dataProps={p} changeListener={this.props.changeListener} />)}</table>)
    }
}

class Row extends React.Component {
    render() {
        if(this.props.dataProps.name === 'color')
        return (
                <tr><td>{this.props.dataProps.name}</td><td><ColorEditor dataProps={this.props.dataProps} changeListener={this.props.changeListener} /></td></tr>
                )
        else
            return (
                    <tr><td>{this.props.dataProps.name}</td><td><TextEditor dataProps={this.props.dataProps} changeListener={this.props.changeListener} /></td></tr>
                    )
    }
}

// https://medium.com/capital-one-tech/how-to-work-with-forms-inputs-and-events-in-react-c337171b923b

class TextEditor extends React.Component {
    constructor(props) {
        super(props)
        this.state = { text: this.props.dataProps.value }
    }
    
    handleChange(event) {
        this.setState({text: event.target.value})
        this.props.changeListener(this.props.dataProps.name, event.target.value)
    }
    
    render() {
        return <input type='text' value={this.state.text} onChange={event => this.handleChange(event) } />
    }
}

class SquareComponent extends React.Component {
    render() {
        const color = this.props.data.color
        const rgb = `rgb(${color[0]},${color[1]},${color[2]})`
        return (
                <div
                style={{background: rgb }}
                className="square">
                { this.props.data.text }
                </div>
                );
    }
}

class ColorEditor extends React.Component {
    constructor(props) {
        super(props)
        this.state = { color: this.props.dataProps.value }
    }
    
    handleChange(event) {
        const color = this.state.color
        if (event.target.name === 'red') color[0] = event.target.value
            else if (event.target.name === 'green') color[1] = event.target.value
                else if (event.target.name === 'blue') color[2] = event.target.value
                    this.setState({color})
                    this.props.changeListener(this.props.dataProps.name, color)
                    }
    
    render() {
        return (
                <div>
                <div><input name='red' type='range' min='0' max='255' value={this.state.color[0]} onChange={event => this.handleChange(event) } /></div>
                <div><input name='green' type='range' min='0' max='255' value={this.state.color[1]} onChange={event => this.handleChange(event) } /></div>
                <div><input name='blue' type='range' min='0' max='255' value={this.state.color[2]} onChange={event => this.handleChange(event) } /></div>
                </div>
                )
    }
}

class Game extends React.Component {
    
    
  
    constructor(props) {
        super(props)
        this.state = { data: new Square([0, 0, 255], 'Fred') }
    }
    
    // https://medium.com/@ruthmpardee/passing-data-between-react-components-103ad82ebd17
    squareChangeListener(name, value) {
        // ...
        this.setState({data: this.state.data})
    }
    
    render() {
        return  (//<SquareComponent data={new Square('blue', 'Fred')}/>
                 <div className="game">
                 <div className="game-board">
                 <SquareComponent data={this.state.data}/>
                 </div>
                 <Sheet dataProps={this.state.data.getProperties()} changeListener=
                 {(n, v) => this.squareChangeListener(n,v)} />
                 </div>
                 );
    }
    
}


// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));

function calculateWinner(squares) {
    const lines = [
                   [0, 1, 2],
                   [3, 4, 5],
                   [6, 7, 8],
                   [0, 3, 6],
                   [1, 4, 7],
                   [2, 5, 8],
                   [0, 4, 8],
                   [2, 4, 6]
                   ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

