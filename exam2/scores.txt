Student: oug
Problem 1

An inheritance relationship (max. 2 points): 2
An 1 : n aggregation (max. 2 points): 0
Wrong direction
An aggregation that is not 1 : n (max. 2 points): 0
That's inheritance
An attribute (max. 2 points): 2
Dependency, not aggregation or inheritance (max. 2 points): 2


Problem 2

process to BoundedMoveStrategy  (max. 2 points): 0
getBounds to BoxedShape, compoundShape  (max. 2 points): 0
getBounds to CarShape, MoveableIcon  (max. 2 points): 0
move to BoxedShape, compoundShape  (max. 2 points): 0
move to CarShape, MoveableIcon  (max. 2 points): 0


Problem 3

Composite

Primitive              | MoveableShape (max. 2 points): 2
Composite              | CompoundShape (max. 2 points): 2
Leaf                   | e.g. CarShape (max. 2 points): 2
method()               | draw, move, getBounds (max. 2 points): 2


Problem 4

text property  (max. 2 points): 0
draw draws string, rectangle  (max. 2 points): 0
getBounds gets rectangle with margins (max. 2 points): 0
getConnectionPoints returns center of bounds (max. 2 points): 0
Prototype (max. 2 points): 1

Problem 5

The template method is drawSelection (max. 2 points): 0
Primitive operation is getBounds (max. 2 points):0
Second scenario not template method  (max. 2 points):0
No self call (max. 2 points): 0

Multiple git commits (max. 4 points): 0
