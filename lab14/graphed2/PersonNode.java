import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;

public class PersonNode extends CircleNode
{
	public PersonNode(Color white, int i)
	{
		super(Color.WHITE, 40);
		setImageURL("http://www.horstmann.com/sjsu/spring2019/cs151/day14/default.png");
	}
	
	public String getImageURL()
	{
		return imageURL;
	}
	
	public void setImageURL(String imageURL)
	{
		this.imageURL = imageURL;
		try
		{
			icon = new ImageIcon(new URL(imageURL));
		}
		catch (IOException ex) 
		{
			
		}
	}
	
	@Override
	public void draw(Graphics2D g2)
	{
		int x = (int) getBounds().getX();
		int y = (int) getBounds().getY();
		icon.paintIcon(null, g2, x, y);
	}
	
	private ImageIcon icon;
	private String imageURL;

}
