
class PebbleGame {
    move(p) {
        if (p > 2) return [p - 2, p - 1]
            else if (p === 2) return [1]
                else return []
                    }
    win(p) {
        return p === 1
    }
}

class NimGame {
    move(p) {
        const result = []
        for (let i = p - 1; 2 * i >= p; i--)
            result.push(i)
            return result
            }
    win(p) {
        return p === 1
    }
}
const myGame = new NimGame()

class GameAnalyzer {
    constructor(game){
        this.game = game
        this.winningMoves = {}
        this.losingPositions = {}
    }
    
    cached(cache, key, value){
            cache[key] = value
            return value
    }
    winningMove(pos) {
        if(this.winningMoves.hasOwnProperty(pos)) return this.winningMoves
        for (const n of this.game.move(pos)) {
            if (this.losingPosition(n)) return this.cached(this.winningMoves,pos,n) // Can force the opponent to lose
                }
        return this.cached(this.winningMoves, pos, undefined) // Couldn't find a winning move
    }
    
    losingPosition(pos) {
        if(this.losingPositions.hasOwnProperty(pos)) return this.losingPositions
        if (this.game.move(pos)) return this.cached(this.losingPositions,pos,true) // Just lost
            for (const n of this.game.move(pos)) {
                if (this.winningMove(n) === undefined) return this.cached(this.losingPositions,pos,false) // Not losing here
                    }
        return this.cached(this.losingPositions, pos, true) // Lose no matter what
    }
}

const myAnalyzer = new GameAnalyzer(myGame)

for (let p = 1; p <= 100; p++) {
    console.log(p + ' -> ' + myAnalyzer.winningMove(p))
}
