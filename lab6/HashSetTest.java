import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

class HashSetTest {

	@Test
	void test() {
		HashSet set = new HashSet(10);
	      set.add(1);
	      set.add(7);
	      set.add(2);
	      set.add(9);
	      
	      Iterator iter = set.iterator();
	      assertEquals(1, iter.next());
	      assertEquals(2, iter.next());
	      assertEquals(7, iter.next());
	      assertEquals(9, iter.next());
	}
	
	 @Test public void testPatternA()
	   {
	      HashSet set = new HashSet(5);
	      set.add(0);
	      set.add(1);
	      set.add(2);
	      set.add(3);
	      set.add(4);
	      Iterator iter = set.iterator();
	      assertEquals(0, iter.next());
	      assertEquals(1, iter.next());
	      assertEquals(2, iter.next());
	      assertEquals(3, iter.next());
	      assertEquals(4, iter.next());
	      assertFalse(iter.hasNext());
	      iter = set.iterator();
	      iter.next();
	      iter.remove();
	      iter = set.iterator();
	      assertEquals(1, iter.next());
	      assertEquals(2, iter.next());
	      assertEquals(3, iter.next());
	      assertEquals(4, iter.next());
	      assertFalse(iter.hasNext());
	   }
	 @Test public void testPatternB()
	   {
	      HashSet set = new HashSet(5);
	      set.add(0);
	      set.add(2);
	      set.add(4);
	      Iterator iter = set.iterator();
	      assertEquals(0, iter.next());
	      
	      assertEquals(2, iter.next());
	     
	      assertEquals(4, iter.next());
	      assertFalse(iter.hasNext());
	      iter = set.iterator();
	      iter.next();
	      iter.remove();
	      iter = set.iterator();
	    
	      assertEquals(2, iter.next());
	      
	      assertEquals(4, iter.next());
	      assertFalse(iter.hasNext());
	   }
	 @Test public void testPatternC()
	   {
	      HashSet set = new HashSet(5);
	      set.add(0);
	      set.add(5);
	      set.add(1);
	      set.add(6);
	      set.add(11);
	      Iterator iter = set.iterator();
	      assertEquals(5, iter.next());
	      assertEquals(0, iter.next());
	      assertEquals(11, iter.next());
	      assertEquals(6, iter.next());
	      assertEquals(1, iter.next());
	      assertFalse(iter.hasNext());
	      iter = set.iterator();
	      iter.next();
	      iter.remove();
	      iter = set.iterator();
	      assertEquals(5, iter.next());
	      assertEquals(1, iter.next());
	      assertEquals(6, iter.next());
	      assertEquals(11, iter.next());
	      assertFalse(iter.hasNext());
	   }

}
