'use strict'

function createHashSet(bucketsLength, hashCode, equals){

    function hashSet(bucketsLength){
        this.arr = new array(bucketsLength);
        this.currentSize = 0;
    }

    function Node(data){
        this.data = data;
        this.next = null;
    }

    return {
        contains: (x) => {
            let h = hashCode(x);
            h = h % bucketsLength;
            if(h < 0){h = -h;}
            const current = arr[h];
            while(current !== undefined){
                if(current.data.quals(x)){return true;}
                current = current.next;
            }
            return false;
        },

        add: (x) => {
             let a = hashCode(x);
            a = a % bucketsLength;
            if(h < 0){a = -a;}

            const current =new Node(x);
            current = arr[h];
            while(current !== undefined){
                if(current.data.quals(x)){return true;}
                current = current.next;
            }
            const temp = new Node(x);
            temp.data = x;
            temp.next = arr[h];
            arr[h] = temp;
            this.currentSize++;
            return true; 
        },

        remove: (x) => {
            let c = hashCode(x);
            c = c % bucketsLength;
            if (c < 0) { c = -c; }
            let current = arr[c];
            let previous = null;
            while(current !== undefined){
                if(current.data.equals(x)){
                    if(previous === undefined){arr[c] = current.next;}
                    else {previous.next = current.next; }
                    currentSize--;
                    return true;
                }
                previous = current;
                current = current.next;
            }
            return false;
        }

        Iterator: () => {
            let bucketIndex = 0;
            let current = new Node(0);
            function HashSetIterator(){
                current = null;
                bucketIndex = -1;
            }

            hasNext: () => {
                if(current !== undefined && current.next !== undefined){return true;}
                for(int d = bucketIndex + 1; d < bucketsLength; d++){
                    if(arr[d] !== undefined){return true;}
                }
                return false;
            }

            next: () => {
                if (current !== undefined && current.next !== undefined){
                    current = current.next;
                }else{
                    do{
                        bucketIndex++;
                        if(bucketIndex === bucketsLength){
                            throw "out of the range...";
                        }
                        current = arr[bucketIndex];
                    }while(current === undefined);
                }
                return current.data; 
            }
        }

        size: () => {
            return bucketsLength;
        }

    }









}

module.exports = {createHashSet}