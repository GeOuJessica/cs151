import java.util.Arrays;
import java.util.Iterator;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class ArraySet implements IntSet
{
   public boolean test(int n)
   {
      if (n < smallest || n > largest) return false;
      for (int i = 0; i < elementCount; i++)
         if (elements[i] == n) return true;
      return false;
   }
   
   public void clear(int n) 
   {
      for (int i = 0; i < elementCount; i++)
         if (elements[i] == n) 
         {
            elements[i] = elements[elementCount - 1];
            modCount++;
            elementCount--;
            if (n == smallest)
            {
               smallest = Integer.MAX_VALUE;
               for (int k = 0; k < elementCount; k++) 
                  smallest = Math.min(elements[k], smallest);
            }
            if (n == largest)
            {
               largest = Integer.MIN_VALUE;
               for (int k = 0; k < elementCount; k++) 
                  largest = Math.max(elements[k], largest);
            }
         }
   }
   
   public void set(int n)
   {
      if (!test(n))
      {
         if (elements == null)
         {
            elements = new int[10];
         }
         else if (elements.length == elementCount)
         {
            elements = Arrays.copyOf(elements, 2 * elements.length);
         }
         elements[elementCount] = n;
         smallest = Math.min(smallest, n);
         largest = Math.max(largest, n);
         elementCount++;
         modCount++;
      }
   }
   
   public int min()
   {
      return smallest;
   }
   
   public int max()
   {
      return largest;
   }

   public int size()
   {
      return elementCount;
   }

   class ArraySetIterator implements Iterator<Integer>
   {
      public ArraySetIterator()
      {
         this.modCount = ArraySet.this.modCount;
         nextIndex = 0;
      }
      
      public Integer next()
      {
         if (!hasNext())
            throw new NoSuchElementException();
         Integer result = elements[nextIndex];
         nextIndex++;
         afterNext = true;
         return result;
      }

      public boolean hasNext()
      {
         if (this.modCount != ArraySet.this.modCount)
            throw new ConcurrentModificationException();
         return nextIndex < elementCount;
      }

      public void remove()
      {
         if (this.modCount != ArraySet.this.modCount)
            throw new ConcurrentModificationException();         
         if (!afterNext)
            throw new IllegalStateException();
         int n = elements[nextIndex - 1];
         elements[nextIndex - 1] = elements[elementCount - 1];
         this.modCount++;
         ArraySet.this.modCount++;
         nextIndex--;
         elementCount--;
         afterNext = false;
         if (n == smallest)
         {
            smallest = Integer.MAX_VALUE;
            for (int k = 0; k < elementCount; k++) 
               smallest = Math.min(elements[k], smallest);
         }
         if (n == largest)
         {
            largest = Integer.MIN_VALUE;
            for (int k = 0; k < elementCount; k++) 
               largest = Math.max(elements[k], largest);
         }
      }

      int modCount;
      int nextIndex;
      boolean afterNext;
   }
   
   public Iterator<Integer> iterator()
   {
      return new ArraySetIterator();
   }
   
   int smallest = Integer.MAX_VALUE;
   int largest = Integer.MIN_VALUE;
   int[] elements;
   int elementCount;
   int modCount;
}
