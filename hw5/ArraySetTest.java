import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArraySetTest
{
   @Test public void iteratorVisitationOrder()
   {      
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      set.set(1001);
      set.set(-2);
      Iterator<Integer> iter = set.iterator();
      assertTrue(iter.hasNext());
      assertEquals((Integer) 100, iter.next());
      assertTrue(iter.hasNext());
      assertEquals((Integer) 99, iter.next());
      assertTrue(iter.hasNext());
      assertEquals((Integer) 1001, iter.next());
      assertTrue(iter.hasNext());
      assertEquals((Integer) (-2), iter.next());
      assertFalse(iter.hasNext());
   }
   
   @Test public void removeTest()
   {
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      set.set(1001);
      set.set(2000);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      iter.next();
      iter.remove();
      iter.next();
      iter.next();
      iter.remove();
      iter = set.iterator();
      assertTrue(iter.hasNext());
      assertEquals((Integer) 100, iter.next());
      assertTrue(iter.hasNext());
      assertEquals((Integer) 2000, iter.next());
      assertFalse(iter.hasNext());      
   }
   
   @Test public void cmeTest()
   {
      ArraySet set = new ArraySet();
      set.set(100);
      set.set(110);
      set.clear(100);
      set.clear(100);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      Iterator<Integer> iter2 = set.iterator();
      iter.remove();
      assertThrows(ConcurrentModificationException.class, () -> iter2.next());
   }
   
   @Test public void removeContract1()
   {
      IntSet set = new ArraySet();
      set.set(100);
      Iterator<Integer> iter = set.iterator();
      assertThrows(IllegalStateException.class, () -> iter.remove());
   }
   
   @Test public void removeContract2()
   {
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      iter.remove();
      assertThrows(IllegalStateException.class, () -> iter.remove());
   }
   
   @Test public void concurrentMod1()
   {
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      set.clear(99);
      assertThrows(ConcurrentModificationException.class, () -> iter.next());
   }
   
   @Test public void concurrentMod2()
   {
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      set.set(-2);
      assertThrows(ConcurrentModificationException.class, () -> iter.next());
   }
   
   @Test public void concurrentMod3()
   {
      IntSet set = new ArraySet();
      set.set(100);
      set.set(99);
      Iterator<Integer> iter = set.iterator();
      iter.next();
      Iterator<Integer> iter2 = set.iterator();
      iter2.next();
      iter.remove();
      assertThrows(ConcurrentModificationException.class, () -> iter2.next());
   }
}
