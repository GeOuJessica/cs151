import java.util.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class IntSeqTest
{
   @Test
   public void fromIteratorTest()
   {
      List<Integer> lst = List.of(1, 4, 9, 16);
      IntSequence seq = IntSequence.fromIterator(lst.iterator());
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 9, seq.next());
      assertEquals((Integer) 16, seq.next());
      assertNull(seq.next());
   }
     
   @Test public void alternateTest1()
   {
      List<Integer> lst1 = List.of(1, 4, 9, 16);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      List<Integer> lst2 = List.of(2, 3, 4, 5);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 2, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 3, seq.next());
      assertEquals((Integer) 9, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 16, seq.next());
      assertEquals((Integer) 5, seq.next());
      assertNull(seq.next());      
   }
   
   @Test public void alternateTest2()
   {
      List<Integer> lst1 = List.of(1, 4, 9, 16);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      List<Integer> lst2 = List.of(2, 3);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 2, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 3, seq.next());
      assertEquals((Integer) 9, seq.next());
      assertEquals((Integer) 16, seq.next());
      assertNull(seq.next());
   }

   @Test public void alternateTest3()
   {
      List<Integer> lst1 = List.of(1, 4);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      List<Integer> lst2 = List.of(2, 3, 4, 5);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 2, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 3, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 5, seq.next());
      assertNull(seq.next());      
   }

   @Test public void alternateTest4()
   {
      IntSequence seq1 = () -> null;
      List<Integer> lst2 = List.of(2, 3, 4, 5);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 2, seq.next());
      assertEquals((Integer) 3, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 5, seq.next());
      assertNull(seq.next());      
   }
   
   @Test public void alternateTest5()
   {
      List<Integer> lst1 = List.of(1, 4, 9, 16);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      IntSequence seq2 = () -> Integer.valueOf(0);
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 9, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 16, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 0, seq.next());
   }
   
   @Test public void alternateInfinite()
   {
      IntSequence seq1 = () -> 1;
      IntSequence seq2 = () -> 0;
      IntSequence seq = seq1.alternate(seq2);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 0, seq.next());
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 0, seq.next());
   }
   
   @Test public void multipleAlternating()
   {
      List<Integer> lst1 = List.of(1, 2, 3, 4);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      List<Integer> lst2 = List.of(5, 6, 7, 8);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq3 = seq1.alternate(seq2);
      IntSequence seq4 = IntSequence.fromIterator(lst1.iterator());
      IntSequence seq5 = IntSequence.fromIterator(lst2.iterator());
      IntSequence seq6 = seq4.alternate(seq5);
            
      assertEquals((Integer) 1, seq3.next());
      assertEquals((Integer) 5, seq3.next());
      assertEquals((Integer) 1, seq6.next());
      assertEquals((Integer) 2, seq3.next());
      assertEquals((Integer) 6, seq3.next());
      assertEquals((Integer) 5, seq6.next());
      assertEquals((Integer) 3, seq3.next());
      assertEquals((Integer) 7, seq3.next());
      assertEquals((Integer) 2, seq6.next());         
   }
   
   @Test public void alternatingAlternating()
   {
      List<Integer> lst1 = List.of(1, 2, 3, 4);
      IntSequence seq1 = IntSequence.fromIterator(lst1.iterator());
      List<Integer> lst2 = List.of(5, 6, 7, 8);
      IntSequence seq2 = IntSequence.fromIterator(lst2.iterator());
      List<Integer> lst3 = List.of(9, 10, 11, 12);
      IntSequence seq3 = IntSequence.fromIterator(lst3.iterator());
      IntSequence seq = seq1.alternate(seq2).alternate(seq3);
      assertEquals((Integer) 1, seq.next());
      assertEquals((Integer) 9, seq.next());
      assertEquals((Integer) 5, seq.next());
      assertEquals((Integer) 10, seq.next());
      assertEquals((Integer) 2, seq.next());
      assertEquals((Integer) 11, seq.next());
      assertEquals((Integer) 6, seq.next());
      assertEquals((Integer) 12, seq.next());
      assertEquals((Integer) 3, seq.next());
      assertEquals((Integer) 7, seq.next());
      assertEquals((Integer) 4, seq.next());
      assertEquals((Integer) 8, seq.next());
      assertNull(seq.next());      
   }

   @Test public void doNotCopy() {
      List<Integer> lst = new ArrayList<>(List.of(1, 4, 9, 16));
      IntSequence seq = IntSequence.fromIterator(lst.iterator());
      assertEquals((Integer) 1, seq.next());
      lst.remove(0);
      assertThrows(ConcurrentModificationException.class,
         () -> seq.next()); 
   }
}
