import java.util.Iterator;

public interface IntSequence
{   
   Integer next();

   static IntSequence fromIterator(Iterator<Integer> iter)
   {
      return () -> iter.hasNext() ? iter.next() : null;
   }

   default IntSequence alternate(IntSequence second)
   {
      boolean[] toggle = new boolean[1];
      return () -> { 
         Integer result = toggle[0] ? second.next() : next();
         toggle[0] = !toggle[0];
         if (result == null) result = toggle[0] ? second.next() : next();
         return result;
      };        
   }
}
