import java.util.Arrays;
import java.util.Iterator;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class BitSet implements IntSet
{
   @Override public boolean test(int n)
   {
      if (elements == null || n < start || n >= start + 32 * elements.length)
         return false;
      int p = (n - start) / 32;
      int i = (n - start) % 32;
      return test(elements[p], i);
   }

   @Override public void set(int n)
   {
      if (elements == null) 
      {
         elements = new int[10];
         start = n;
      }
      else if (n < start)
      {
         int intsNeeded = (start + 32 * elements.length - n) / 32 + 1;
         int[] newElements = new int[Math.max(intsNeeded, 2 * elements.length)];
         System.arraycopy(elements, 0, 
               newElements, newElements.length - elements.length, 
               elements.length);
         start -= 32 * (newElements.length - elements.length);
         elements = newElements;
      }
      else if (n >= start + 32 * elements.length)
      {
         int intsNeeded = (n - start) / 32 + 1; 
         elements = Arrays.copyOf(elements, 
               Math.max(intsNeeded, 2 * elements.length));         
      }
      int p = (n - start) / 32;
      int i = (n - start) % 32;
      if (!test(elements[p], i)) 
      {
         elementCount++;
         modCount++;
         elements[p] = set(elements[p], i);      
      }
   }

   @Override public void clear(int n)
   {
      if (elements != null && n >= start || n < start + 32 * elements.length)
      {
         int p = (n - start) / 32;
         int i = (n - start) % 32;
         if (test(elements[p], i)) 
         {
            elementCount--;
            modCount++;
            elements[p] = clear(elements[p], i);
         }
      }
   }

   @Override public int min()
   {
      if (elements != null)
         for (int p = 0; p < elements.length; p++)
            for (int i = 0; i < 32; i++)
               if (test(elements[p], i)) return 32 * p + i + start;      
      return Integer.MAX_VALUE;
   }

   @Override public int max()
   {
      if (elements!= null)
         for (int p = elements.length - 1; p >= 0; p--)
            for (int i = 31; i >= 0; i--)
               if (test(elements[p], i)) return 32 * p + i + start;
      return Integer.MIN_VALUE;
   }

   @Override public int size()
   {
      return elementCount;
   }

   private static boolean test(int n, int i)
   {
      assert 0 <= i && i < 32;
      return (n & (1 << i)) != 0;
   }

   private static int set(int n, int i)
   {
      assert 0 <= i && i < 32;
      return n | (1 << i);
   }

   private static int clear(int n, int i)
   {
      assert 0 <= i && i < 32;
      return n & ~(1 << i);
   }

   private class BitSetIterator implements Iterator<Integer>
   {
      public BitSetIterator()
      {
         this.modCount = BitSet.this.modCount;
         nextIndex = 0;
         nextBit = -1;
         moveToNext();
      }

      private void moveToNext()
      {
         boolean done = nextIndex == elements.length;
         while (!done)
         {
            nextBit++;
            if (nextBit == 32)
            {
               nextIndex++;
               nextBit = 0;
            }
            done = nextIndex == elements.length
               || test(elements[nextIndex], nextBit);
         }
      }
      
      public Integer next()
      {
         if (!hasNext())
            throw new NoSuchElementException();         
         Integer result = start + 32 * nextIndex + nextBit;
         moveToNext();
         removed = result;
         afterNext = true;
         return result;
      }

      public boolean hasNext()
      {
         if (this.modCount != BitSet.this.modCount)
            throw new ConcurrentModificationException();
         return nextIndex < elements.length;
      }

      public void remove()
      {
         if (this.modCount != BitSet.this.modCount)
            throw new ConcurrentModificationException();         
         if (!afterNext)
            throw new IllegalStateException();
         clear(removed);
         this.modCount++;
         elementCount--;
         afterNext = false;
      }

      private int modCount;
      private int nextIndex;
      private int nextBit;
      private int removed;
      private boolean afterNext;
   }
   
   @Override public Iterator<Integer> iterator()
   {
      return new BitSetIterator();
   }

   int[] elements;
   int start;
   int elementCount;
   int modCount;
}
