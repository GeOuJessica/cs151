'use strict'

const findNode = require('./graph4');

let n1 = {x: 10 , y: 40, radius: 20, color: 'white', 
type: 'simple'}

let n2 = {x: 10 , y: 40, radius: 20, color: 'white', 
type: 'simple'}

let e = {start: n1, end: n2}

let graph = {nodes:[n1, n2], edges:[e]}

let result = findNode(graph, 10, 40)

test('we find a white node at 10, 40', ()=>{
	expect(result.color).toBe('white');
})
