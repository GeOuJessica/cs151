'use strict'

// let n1 = {x: 10 , y: 40, radius: 20, color: 'white', 
// type: 'simple'}

// let n2 = {x: 10 , y: 40, radius: 20, color: 'white', 
// type: 'simple'}

// let e = {start: n1, end: n2}

// let graph = {nodes:[n1, n2], edges:[e]}

function findNode(g, x, y){
	for(const n of g.nodes){
		if(n.x === x && n.y === y) return n
	}
	return undefined
}

// console.log(JSON.stringify(graph))

// console.log(findNode(graph, 10, 40))

module.exports = findNode;