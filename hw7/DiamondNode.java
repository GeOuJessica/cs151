import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * A diamond shaped node class
 */
public class DiamondNode implements Node{
   private static final int DEFAULT_SIZE = 20;
   private double x;
   private double y;
   private double size;
   private Color color;

   public DiamondNode() {
      size = DEFAULT_SIZE;
      x = 0;
      y = 0;
   }

   public Object clone() {
      try {
         return super.clone();
      }catch(CloneNotSupportedException e) {
         return null;
      }
   }

   @Override
   public void draw(Graphics2D g2) {
      g2.draw(new Line2D.Double(x + size/2, y, x+size, y+size/2));
      g2.draw(new Line2D.Double(x + size/2, y, x, y+size/2));
      g2.draw(new Line2D.Double(x + size/2, y+size, x+size, y+size/2));
      g2.draw(new Line2D.Double(x+size/2, y+size, x, y+size/2));
   }

   @Override
   public void translate(double dx, double dy) {
      x = x + dx;
      y = y + dy;
   }

   @Override
   public boolean contains(Point2D aPoint) {
      return getBounds().contains(aPoint);
   }

   @Override
   public Point2D getConnectionPoint(Point2D aPoint) {
      double centerX = x + size/2;
      double centerY = y + size/2;
      double dx = aPoint.getX() - centerX;
      double dy = aPoint.getY() - centerY;
      if(dx >= dy && dx >= -dy) {
         return new Point2D.Double(x+size, centerY);
      }

      if (dx < dy && dx >= -dy) {
         return new Point2D.Double(centerX, y + size);
      }

      if(dx >= dy && dx < -dy) {
         return new Point2D.Double(centerX, y);
      }

      return new Point2D.Double(x, centerY);
   }

   @Override
   public Rectangle2D getBounds() {
      return new Rectangle2D.Double(x, y, size, size);
   }

}
