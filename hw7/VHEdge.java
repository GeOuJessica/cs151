import java.awt.*;
import java.awt.geom.*;
import java.io.*;

public class VHEdge extends AbstractEdge{
	public VHEdge() {
		lineStyle = LineStyle.SOLID;
	}

	@Override
	public void draw(Graphics2D g2) {
		Stroke oldStroke = g2.getStroke();
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	    		RenderingHints.VALUE_ANTIALIAS_ON);
	    Shape drawLine = new Line2D.Float(2, -2, 2, -2);
	    g2.draw(getConnectionPoints());
	    g2.setStroke(oldStroke);
	}

	@Override
	public boolean contains(Point2D aPoint) {
		final double MAX_DIST = 2;
	    return getConnectionPoints().ptSegDist(aPoint) < MAX_DIST;
	}
	   /**
    Sets the line style property.
    @param newValue the new value
 */
 public void setLineStyle(LineStyle newValue) { lineStyle = newValue; }

 /**
    Gets the line style property.
    @return the line style
 */
 public LineStyle getLineStyle() { return lineStyle; }
 
 private LineStyle lineStyle;
}
