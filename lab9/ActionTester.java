javaimport java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ActionTester
{
    static int counter = 0;
    
   public static void main(String[] args)
   {
      JFrame frame = new JFrame();

      final int FIELD_WIDTH = 20;
      final JTextField textField = new JTextField(FIELD_WIDTH);
      textField.setText("Click a button!");

      JButton helloButton = new JButton("Say Hello");
      JButton goodbyeButton = new JButton("Say Goodbye");

       helloButton.addActionListener(event ->{
           textField.setText("Hello, World!")
           helloBoutton.setEnabled(false);
       });

      

      goodbyeButton.addActionListener(event ->
         textField.setText("Goodbye, World!"));

      frame.setLayout(new FlowLayout());

      frame.add(helloButton);
      frame.add(goodbyeButton);
      frame.add(textField);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);
   }
}
